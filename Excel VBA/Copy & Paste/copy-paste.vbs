'Be aware that `Range.Copy` and `Range.PasteSpecial` use the clipboard.

'copy/paste values only (without using the clipboard)
Private Function CopyPasteValues(fromRange As Range, toCell As Range) As Range
    
    Dim rows As Long, columns As Long, toRange As Range
    
    rows = fromRange.rows.Count
    columns = fromRange.columns.Count
    
    Set toRange = toCell.Worksheet.Range(toCell, toCell.Offset(rows - 1, columns - 1))
    
    toRange.Value = fromRange.Value
    
    Set CopyPasteValues = toRange
    
End Function

'copy/paste everything (values, formats, comments, et al.; uses the clipboard)
Private Sub CopyPaste(fromRange As Range, toRange As Range)
    
    fromRange.Copy toRange
    
End Sub

'use Paste Special to be more specific (uses the clipboard)
Private Sub CopyPasteSpecial(fromRange As Range, toRange As Range, _
 Optional pasteType As XlPasteType = xlPasteAll, _
 Optional pasteOperation As XlPasteSpecialOperation = xlPasteSpecialOperationNone, _
 Optional skipBlanks As Boolean = False, _
 Optional transpose As Boolean = False)
    
    fromRange.Copy
    Call toRange.PasteSpecial(pasteType, pasteOperation, skipBlanks, transpose)
    
End Sub